<?php

//Resource Routes..
Route::group([
    'prefix' => 'app'
], function () {
    // Resource Routes..
    Route::apiResources([
        'projects' => 'ProjectController',
    ]);
});

// Any - Vue Routes..
Route::get('{any}', 'VueController')->where('any', '^(?!nova|app.*$).*');
