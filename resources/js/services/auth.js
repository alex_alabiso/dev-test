import Store from '../store';

export default {
    /**
     * Returns true if the user is logged in.
     *
     * @return {boolean}
     */
    isLoggedIn() {
        return true;
    }
};
