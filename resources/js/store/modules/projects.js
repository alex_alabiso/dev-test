import * as types from '../types';
import axios from 'axios';

const state = {
    all: null
};

const getters = {
    projects: state => state.all
};

const actions = {
    /**
     * Loads Projects from the backend.
     *
     * @param commit
     * @returns {Promise}
     */
    projectsLoad({ commit }) {
        return axios.get(`/app/projects`).then(response => {
            commit(types.PROJECTS_LOADED, response.data);
            return response;
        });
    },

    /**
     * Creates a new project
     *
     * @param commit
     * @param form
     * @return {Promise<T | never>}
     */
    createProject({ commit }, { form }) {
        console.log('loading');
        return form.post('/app/projects').then(response => {
            commit(types.PROJECT_CREATED, response.data);

            return response;
        });
    },

    /**
     * Updates a project
     *
     * @param commit
     * @param form
     * @return {Promise<T | never>}
     */
    updateProject({ commit }, { form, id }) {
        return form.put(`/app/projects/${id}`).then(response => {
            commit(types.PROJECT_UPDATED, response.data);
            return response;
        });
    },


    /**
     * Deletes a project
     *
     * @param commit
     * @param form
     * @return {Promise<T | never>}
     */
    deleteProject({ commit }, { form, id }) {
        return form.delete(`/app/projects/${id}`).then(response => {
            commit(types.PROJECT_DELETED, response.data);
            return response;
        });
    },


};

const mutations = {
    /**
     * Mutates loaded Projects into the state.
     *
     * @param state
     * @param data
     */
    [types.PROJECTS_LOADED](state, data) {
        if (state.all === null) {
            state.all = data;
        } else {
            state.all = state.all.concat(data);
        }
    },

    /**
     * Mutates new Project into the state
     *
     * @param state
     * @param data
     */
    [types.PROJECT_CREATED](state, data) {
        if (state.all === null) {
            return;
        }
        state.all.unshift(data);
    },

    [types.PROJECT_UPDATED](state, data) {
    },

    [types.PROJECT_DELETED](state, data) {
    }
};

export default {
    state,
    getters,
    actions,
    mutations
};
