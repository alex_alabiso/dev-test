import Vue from 'vue';
import Vuex from 'vuex';
import store from './store';
import router from './router';
import Buefy from 'buefy';

Vue.use(Buefy, {
    defaultIconPack: "fas",
    store: store
});

Vue.use(Vuex);

require('./bootstrap');

/**
 * Finally, we will boot our application.
 */
new Vue({
    router,
    store,
    components: {
        app: require('./views/App')
    }
}).$mount('#app');
