import Vue from 'vue';
import Routes from './routes';
import VueRouter from 'vue-router';

Vue.use(VueRouter);

const Router = new VueRouter({
    routes: Routes,
    mode: 'history',
    linkActiveClass: 'is-active',
    scrollBehavior(to, from, savedPosition) {
        if (savedPosition) {
            return savedPosition;
        } else if (to.hash) {
            return { selector: to.hash };
        } else {
            return { x: 0, y: 0 };
        }
    }
});

// DO NOT ALTER ORDER
// These fire in a very specific order to ensure the application load doesn't
// throw a fit.


// These guards are here to ensure the user has the proper account attributes
// to use the application.
// Router.beforeEach(SubscriptionGuard); TODO will be used once Stripe implemented.

export default Router;
