export default [
    {
        path: '/',
        name: 'home',
        component: require('../views/App.vue')
    },

    // 404
    {
        path: '*',
        name: 'error.404',
        component: require('../views/Errors/NotFound.vue')
    }
];
