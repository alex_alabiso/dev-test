window._ = require('lodash');
window.Promise = require('promise');
window.axios = require('axios');
window.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest'
};

import Form from './core/Form';
window.Form = Form;
require('./vue/filters');
require('./vue/components');
