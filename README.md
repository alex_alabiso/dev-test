## Instructions

This exercise is designed to test your abilities as a full stack developer. It should take around 3-4 hours for an experienced developer to complete. It will require knowledge of Laravel and Vue. Before getting started you'll want to take a look at the following files/classes:

* App/Http/Controller/ProjectController
* App/Repositories/ProjectRepository
* App/Models/Project
* App/Http/Requests/
* app/routes/web.php
* database/migrations/2018_12_12_134347_create_projects_table.php
* resources/js/store (See how modules/types are being used, some of the stuff for projects is already there so please follow that style)

## Installation

* Setup your .env file
* Create your database
* `npm install`
* `composer install`
* `php artisan migrate`
* `php artisan serve` (or use valet)

## Tasks

1. Create a CRUD interface for the Project resource starting with the Projects table
1. Projects will need to have the following fields:
    * Name
    * Description
    * Due Date
    * Status (preferably normalized in the DB: "active", "inactive", "pending", "archived")
    * Timestamps/Softdelete
1. Controllers should be thin, using the ProjectRepository to store the data via the Project model
1. Please use the Requests in app/Http/Requests/* and use them to validate your data (Don't worry about authentication)
1. Forms should be validated and errors shown on the form
1. Creating/updating a project can either happen via modal or a new page (up to you) but you should use the same component for both (remember this is a SPA, so once created or updated it needs to show up in the table without refreshing)
1. Docs for the table you're populating (and other components) can be found <a href="https://buefy.github.io/#/documentation/table">here</a>. Bonus points if you can implement pagination and sorting.

## Guidelines
* Keep it simple
* Don't repeat yourself
* A lot of the "work" is already done for you, follow the partially completed code and fill in the blanks as you see fit
* Feel free to use any composer or npm libraries to make your life easier
* Adherence to basic coding standards and best practices for PHP, JS and MySQL
* We're looking for a few different things here so don't sweat it if you don't complete everything. We're looking not so much as **_what_** you do as **_how_** you do it. If you get stuck on something just move on to the next. 

## Submission of Work
1. Create a new branch from master
1. Commit your code with one or more clean commit messages explaining what you did
1. Create a pul request for your branch



