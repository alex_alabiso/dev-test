const { mix } = require('laravel-mix');

/**
 * Configuration
 */
// mix.version();
mix.sourceMaps();

if (process.env.NODE_ENV === 'production') {
    mix.disableNotifications();
}

/**
 * SCSS Compilation
 */
mix.sass('resources/sass/app.scss', 'public/css');

/**
 * JavaScript Compilation
 *
 * > Compilation
 * @see https://github.com/JeffreyWay/laravel-mix/blob/master/docs/mixjs.md
 *
 * > Extraction
 * @see https://github.com/JeffreyWay/laravel-mix/blob/master/docs/extract.md
 */
mix.js('resources/js/app.js', 'public/js').extract([
    'axios',
    'lodash',
    'vue',
    'vue-router',
    'vuex',
]);

