<?php

namespace App\Repositories;

use App\Models\Project;
use App\Http\Requests\Projects\StoreRequest;

class ProjectRepository
{
    /**
     * @var string
     */
    protected $model = Project::class;

    /**
     * Returns all the Projects
     *
     * @return \App\Models\Project[]|\Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return Project::all();
    }

    /**
     * Returns a single Project
     *
     * @param $id
     * @return mixed
     */
    public function show($id)
    {

    }

    /**
     * Creates a Project
     *
     * @param \App\Http\Requests\Projects\StoreRequest $request
     */
    public function create(StoreRequest $request)
    {

    }

}
