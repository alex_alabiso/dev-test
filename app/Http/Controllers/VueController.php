<?php

namespace App\Http\Controllers;

class VueController extends Controller
{
    /**
     * VueController invoker..
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function __invoke()
    {
        return view('vue');
    }
}
